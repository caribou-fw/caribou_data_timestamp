`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 25.09.2018 14:36:59
// Design Name: 
// Module Name: data_timestamp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module timestamp_sync #(
    int TS_WIDTH = 48,
    int SYNC_STAGES = 2
  )(
    input clk_data,
    input rst_Cdata,
    input clk_ts,
    input t0_Async,
    input conf_t0_negative,
    input t0_flag_clear_Cdata,
    output [TS_WIDTH-1:0] timestamp_Cdata,
    output t0_out_Cdata
  );

  logic [TS_WIDTH-1:0] ts_reg_Cts = 0;
  logic [TS_WIDTH-1:0] ts_reg_capture_Cts = 0;
  logic [TS_WIDTH-1:0] ts_reg_capture_Cdata = 0;

  logic t0_in_Cts;
  logic t0_event_Cts;
  logic t0_event_capture_Cts = 1'b0;
  logic t0_event_capture_Cdata = 1'b0;
  logic t0_event_hold_Cdata;

  logic capture_data_Cts;
  logic capture_data_Cdata;
  logic capture_data_Cdata_force0;
  logic capture_data_Cdata_force1;
  logic capture_data_Cdata_syncout;

  logic [3:0] reset_countdown = 4'b1;
  logic       reset_hold;

  assign timestamp_Cdata = ts_reg_capture_Cdata;
  assign t0_out_Cdata = t0_event_hold_Cdata;

  assign capture_data_Cdata = ~capture_data_Cdata_force0 && (capture_data_Cdata_syncout || capture_data_Cdata_force1);
  
  always_ff @(posedge clk_data) begin
    if (rst_Cdata) begin
      reset_countdown = 4'b1;
      capture_data_Cdata_force0 = 1'b1;
      capture_data_Cdata_force1 = 1'b0;
    end
    else begin
      if (reset_hold) begin
        reset_countdown <= reset_countdown - 1;
        capture_data_Cdata_force0 = 1'b1;
        capture_data_Cdata_force1 = 1'b0;
      end
      else if (capture_data_Cdata_force0) begin
        reset_countdown <= reset_countdown;
        capture_data_Cdata_force0 = 1'b0;
        capture_data_Cdata_force1 = 1'b1;
      end
      else begin
        reset_countdown <= reset_countdown;
        capture_data_Cdata_force0 = 1'b0;
        capture_data_Cdata_force1 = 1'b0;
      end
    end
  end
  assign reset_hold = (| reset_countdown) ? 1'b1 : 1'b0;
  
  // sync T0 input to ts clock and inverse in case of negative polarity
  sync_reset #(
    .STAGES(SYNC_STAGES) 
  ) t0_sync (
    .negative(conf_t0_negative),
    .clk(clk_ts),
    .arst_in(t0_Async),  // i, async 
    .rst_out(t0_in_Cts)   // o, sync to clk
  );

  // sync pulses DIN -> TS clock domain
  sync_pulse #(
    .WIDTH(1),
    .STAGES(SYNC_STAGES) 
  ) data_sync_Cdata_Cts (
    .in_clk(clk_data),
    .out_clk(clk_ts),
    .pulse_in(capture_data_Cdata),  // i, sync to in_clk 
    .pulse_out(capture_data_Cts) // o, sync to out_clk
  );  
  // sync pulses TS -> DIN clock domain
  sync_pulse #(
    .WIDTH(1),
    .STAGES(SYNC_STAGES) 
  ) data_sync_Cts_Cdata (
    .in_clk(clk_ts),
    .out_clk(clk_data),
    .pulse_in(capture_data_Cts),  // i, sync to in_clk 
    .pulse_out(capture_data_Cdata_syncout) // o, sync to out_clk
  );

  // timestamp counter
  always_ff @(posedge clk_ts) begin
    if (t0_in_Cts)
      ts_reg_Cts <= 47'b0;
    else
      ts_reg_Cts <= ts_reg_Cts + 1;
  end

  // T0 release edge detection on Cts side
  edge_detect #(
    .RISING(0),
    .FALLING(1)
  ) t0_edge_Cts (
    .clk(clk_ts),
    .rst(1'b0),
    .sig_in(t0_in_Cts),
    .clear_flag(capture_data_Cts),
    .edge_detected(t0_event_Cts)
  );

  // data propagation through clock domains Cts -> Cdata
  //   data capture - Cts domain
  always_ff @(posedge clk_ts) begin
    if (capture_data_Cts) begin
      ts_reg_capture_Cts <= ts_reg_Cts;
      t0_event_capture_Cts <= t0_event_Cts;
    end
    else begin
      ts_reg_capture_Cts <= ts_reg_capture_Cts;
      t0_event_capture_Cts <= t0_event_capture_Cts;
    end
  end

  // data propagation through clock domains Cts -> Cdata
  // data capture - Cdata domain
  always_ff @(posedge clk_data) begin
    if (reset_hold) begin
      ts_reg_capture_Cdata <= 0;
      t0_event_capture_Cdata <= 0;
    end
    else begin
      if (capture_data_Cdata) begin
        ts_reg_capture_Cdata <= ts_reg_capture_Cts;
        t0_event_capture_Cdata <= t0_event_capture_Cts;
      end
      else begin
        ts_reg_capture_Cdata <= ts_reg_capture_Cdata;
        t0_event_capture_Cdata <= t0_event_capture_Cdata;
      end
    end
  end

  // T0 detection on Cdata side
  edge_detect #(
    .RISING(1),
    .FALLING(0)
  ) t0_edge_Cdata (
    .clk(clk_data),
    .rst(reset_hold),
    .sig_in(t0_event_capture_Cdata),
    .clear_flag(t0_flag_clear_Cdata),
    .edge_detected(t0_event_hold_Cdata)
  );

endmodule
