`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 25.09.2018 14:36:59
// Design Name: 
// Module Name: edge_detect
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module edge_detect #(
    logic RISING = 0,
    logic FALLING = 1
  )(
    input clk,
    input rst,
    input sig_in,
    input clear_flag,
    output edge_detected
  );

  logic sig_in_prev;
  logic edge_hit;
  logic edge_hit_hold = 0;

  
  assign edge_hit = ((~sig_in_prev && sig_in) && RISING) || ((sig_in_prev && ~sig_in) && FALLING);
  assign edge_detected = (edge_hit_hold || edge_hit) && (~rst);

  always_ff @(posedge clk) begin
    sig_in_prev <= sig_in;
  end

  always_ff @(posedge clk) begin
    if (rst) begin
      edge_hit_hold <= 1'b0;
    end
    else begin
      if (clear_flag)
        edge_hit_hold <= 1'b0;
      else if (edge_detected)
        edge_hit_hold <= 1'b1;
      else
        edge_hit_hold <= edge_hit_hold;
    end
  end


endmodule
