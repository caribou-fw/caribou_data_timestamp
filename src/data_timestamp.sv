`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 25.09.2018 14:36:59
// Design Name: 
// Module Name: data_timestamp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module data_timestamp (
    input clk_din,
    input rst,
    input din_valid,
    input [31:0] din,
    input din_frame_start,
    input conf_t0_negative,
    input [4:0] status_1,
    input [4:0] status_2,

    input clk_ts,
    input t0_in,
    output dout_write,
    output [63:0] dout
  );

  

  logic [23:0] ts_dout_sig;
  logic [7:0] status_dout_sig;

  logic [47:0] ts_sig;
  logic [23:0] ts_msb_reg;

  logic t0_clear;
  logic t0_sig;
  logic ts_msb_changed; // notifies, that MSB of timestamp was changed since last time it was sent
  logic ts_msb_change_reg = 1'b1;
  logic ts_msb_change_set;
  logic ts_msb_change_clear;
  
  logic dout_write_sig;

  logic switch_ts_state_reg; 
  logic switch_ts_state_sig; // selects which part of timestamp is being send

  timestamp_sync #(
    .TS_WIDTH(48),
    .SYNC_STAGES(2)
  ) timestamp_inst (
    .clk_data(clk_din),
    .rst_Cdata(rst),
    .clk_ts(clk_ts),
    .t0_Async(t0_in),
    .conf_t0_negative(conf_t0_negative),
    .t0_flag_clear_Cdata(t0_clear),
    .timestamp_Cdata(ts_sig),
    .t0_out_Cdata(t0_sig)
  );

  assign ts_dout_sig = switch_ts_state_sig ? ts_msb_reg[23:0] : ts_sig[23:0];
  assign status_dout_sig = switch_ts_state_sig ? {3'b100, status_2} : {1'b0, ts_msb_changed, t0_sig, status_1};
  assign dout_write = dout_write_sig;
  assign dout_write_sig = din_valid;
  assign dout = {status_dout_sig, ts_dout_sig, din};

  assign t0_clear = (~switch_ts_state_sig) && dout_write_sig;

  assign ts_msb_change_set = (ts_msb_reg[0] ^ ts_sig[24]);
  assign ts_msb_change_clear = switch_ts_state_sig && dout_write_sig;
  assign ts_msb_changed = ts_msb_change_set || ts_msb_change_reg;
  
  always_ff @(posedge clk_din) begin
    if (rst) begin
      ts_msb_change_reg <= 1'b1;
    end
    else if (ts_msb_change_clear) begin
      ts_msb_change_reg <= 1'b0;
    end
    else if (ts_msb_change_set) begin
      ts_msb_change_reg <= 1'b1;
    end
    else begin
      ts_msb_change_reg <= ts_msb_change_reg;
    end
  end
   
  // manage MSB or LSB selection
  assign switch_ts_state_sig = switch_ts_state_reg && (~din_frame_start);

  always_ff @(posedge clk_din) begin
    if (rst) begin
      switch_ts_state_reg <= 1'b0;
    end
    else if (dout_write_sig)
      switch_ts_state_reg <= ~switch_ts_state_sig;
    else
      switch_ts_state_reg <= switch_ts_state_sig;
  end
  // --

  // hold timstamp MSB when writing LSB
  always_ff @(posedge clk_din) begin
    if (!switch_ts_state_sig) 
      ts_msb_reg[23:0] <= ts_sig[47:24];
    else begin
      ts_msb_reg[23:0] <= ts_msb_reg[23:0];
    end
  end
  // --  

endmodule
