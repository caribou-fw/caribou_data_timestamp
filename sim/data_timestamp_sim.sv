`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.10.2018 11:13:40
// Design Name: 
// Module Name: data_timestamp_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module data_timestamp_sim(
    );

  logic clk_din;
  logic rst = 0;
  logic din_valid = 0;
  logic [31:0] din = 0;
  logic din_frame_start = 0;
  logic conf_t0_negative;
  logic [4:0] status_1;
  logic [4:0] status_2;

  logic clk_ts;
  logic t0_in = 0;
  logic dout_write;
  logic [63:0] dout;

  logic tb_start = 1'b0;
  logic [3:0] tb_rand_sig;
  logic [31:0] tb_ts_counter = 0;
    
  logic stat_t0_was = 0;
  logic stat_msb_valid = 0;
  logic [63:0] stat_counter;
  logic [31:0] stat_data;
  logic [31:0] stat_diff;

  logic clk_din_write;
  logic clk_din_read;   
  logic clk_ts_write;
  logic clk_ts_read;
  
// Connect DUT
data_timestamp data_timestamp_inst (
    .clk_din(clk_din),
    .rst(rst),
    .din_valid(din_valid),        
    .din(din),
    .din_frame_start(din_frame_start),        
    .conf_t0_negative(conf_t0_negative),
    .status_1(status_1),
    .status_2(status_2),
    
    .clk_ts(clk_ts),
    .t0_in(t0_in),
    .dout_write(dout_write),
    .dout(dout)
  );


  parameter CLK_DIN_PERIOD = 25.0; // 25ns ~ 40 MHz
  parameter CLK_DIN_HOLD = 2; 
  parameter CLK_DIN_SETUP = 2; 

  parameter CLK_TS_PERIOD = 6.25; // 6.25ns ~ 160 MHz
  parameter CLK_TS_HOLD = 2; 
  parameter CLK_TS_SETUP = 2; 

  assign conf_t0_negative = 1'b0;
  assign status_2 = 5'b10101;
  assign status_1 = 5'b01010;

  // **********************************************************************
  // generate clock
  // clk for tested logic
  always begin
    clk_din = 1'b0;
    #(CLK_DIN_PERIOD/2) clk_din = 1'b1;
    #(CLK_DIN_PERIOD/2);
  end

  always begin
    clk_ts = 1'b0;
    #(CLK_TS_PERIOD/2) clk_ts = 1'b1;
    #(CLK_TS_PERIOD/2);
  end

  // clk for stimulus write 
  initial begin
    clk_din_write = 1'b0;
    #CLK_DIN_HOLD;
      forever begin
        clk_din_write = ~clk_din_write;
        #(CLK_DIN_PERIOD/2);
      end
  end

  initial begin
    clk_ts_write = 1'b0;
    #CLK_TS_HOLD;
      forever begin
        clk_ts_write = ~clk_ts_write;
        #(CLK_TS_PERIOD/2);
      end
  end
  
  // clk for output read 
  initial begin
    clk_din_read = 1'b0;
    #((CLK_DIN_PERIOD/2)-CLK_DIN_SETUP);
      forever begin
        clk_din_read = ~clk_din_read;
        #(CLK_DIN_PERIOD/2);
      end
  end

  initial begin
    clk_ts_read = 1'b0;
    #((CLK_TS_PERIOD/2)-CLK_TS_SETUP);
      forever begin
        clk_ts_read = ~clk_ts_read;
        #(CLK_TS_PERIOD/2);
      end
  end
  
  // END generate clock 
  // **********************************************************************

  // **********************************************************************
  // generate test vectors
   
  // generate resets
  initial begin
    t0_in = 1'b0;
    rst =  1'b1;
    # CLK_DIN_PERIOD;
    wait(clk_din==1'b0);
    wait(clk_din==1'b1);
    # CLK_DIN_HOLD; rst = 1'b0;
    # 200 t0_in = 1'b1;
    # 15 t0_in = 1'b0;
  end

  always @(posedge clk_ts) begin
    if (t0_in == 1'b1)
      tb_ts_counter <= 0;
    else
      tb_ts_counter <= tb_ts_counter + 1;
  end

  always @(posedge clk_din_write) begin
    tb_rand_sig = $random();
    din <= tb_ts_counter;
    if (rst) begin 
      din_valid <= 1'b0;
      din_frame_start <= 1'b0;
    end
    else begin
      din_valid <= &tb_rand_sig[1:0];
      din_frame_start <= &tb_rand_sig;
    end
  end
  // END generate test vectors
  // **********************************************************************

  // **********************************************************************
  // read and evaluate the data output 
  always @(posedge clk_din_read) begin
    if (din_valid) begin
      // counter LSB
      if (!dout[63]) begin
        if (dout[61]) begin
          stat_t0_was <= 1'b1;
        end
        if (dout[62]) begin
          stat_counter[63:24] = 0;
          stat_msb_valid = 1'b0;
        end
        stat_counter[23:0] = dout[55:32];
      end
      // counter MSB
      else begin
        //stat_counter[23:0] = 0;
        stat_counter[63:24] = dout[55:32];
        stat_msb_valid = 1'b1;
      end
      stat_data = dout[31:0];
    end
    
    
    if (din_valid && stat_t0_was) begin
      stat_diff = stat_data[31:0] - stat_counter[31:0];
      if (!stat_msb_valid) begin
        stat_diff[31:24] = 0;
      end

      if (stat_diff > 250) begin 
        $error("Timestamp shift too big");
      end
      if (stat_data != din) begin
        $error("DIN and DOUT does not match");
      end
    end
    
    
  end
  // END read and evaluate the data output
  // **********************************************************************

endmodule
